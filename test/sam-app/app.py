# lambda function
import json

def hello(event, context):
    with open('./config.json', 'r') as f:
        message = json.loads(f.read()).get('message')
        return {
            "statusCode": 200,
            "body": json.dumps({"message": message}),
        }
