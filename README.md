# Bitbucket Pipelines Pipe: AWS Lambda Deploy
Update Lambda function code and create new version, and create/update aliases pointing to versions.


## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-lambda-deploy:0.5.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    FUNCTION_NAME: '<string>'
    COMMAND: '<string>' # 'alias' or 'update'
    # Update variables
    # ZIP_FILE: '<string>'

    # Alias variables
    # ALIAS: '<string>'
    # VERSION: '<string>'
    # FUNCTION_CONFIGURATION: "<string>" # Optional

    # Common variables
    # DEBUG: '<boolean>' # Optional.
```

## Variables
The following variables are required:

| Variable | Usage |
| ----------- | ----- |
| AWS_ACCESS_KEY_ID (**)     | AWS access key. |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key. |
| AWS_DEFAULT_REGION (**)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html) in the _Amazon Web Services General Reference_. |
| FUNCTION_NAME (*)         | Name or ARN of the function. |
| COMMAND (*)               | The operation to perform. Valid options are 'update' or 'alias'. |
| ZIP_FILE (*)              | Path to the zip file containing the function code. Required for 'update' |
| ALIAS (*)                 | Alias to create or update. Required for 'alias'. |
| VERSION (*)               | The version of the function that the alias will point to. Required for 'alias'. If not provided, the pipe will try to fetch this version automatically (only available after you run the pipe in `update` mode in one of the previous steps).| 
| FUNCTION_CONFIGURATION    | Path to a json file containing an function configuration parameters to update. Default: `null`. If provided, the pipe will update the function configuration. The format of the json file is the same as for [update-function-configuration](https://docs.aws.amazon.com/cli/latest/reference/lambda/update-function-configuration.html) with the `--cli-input-json` option. |
| DEBUG                     | Turn on extra debug information. Default: `false`.|
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Details
### Update Lambda Code
The pipe will update the named Lambda function with the code contained within a provided zip file, and atomically publish a new version. Both the new version 
and the built in $LATEST version will point to the updated Lambda.

The function ARN and version will be written to the file 'pipe.meta.env' as <key>=<value> pairs once the pipe has completed successfully.


### Create or Update Lambda Alias
Create or update an alias to point to a specific version of a Lambda function.


## Prerequisites
For Lambda updates, you will need:

  * An existing Lambda function
  * An IAM user configured with programmatic access that can perform the following operations on your function:
    * lambda:PublishVersion
    * lambda:UpdateFunctionCode

For Lambda alias, you will need:

  * An existing Lambda function
  * An IAM user has been configured with programmatic access that can perform the following operations on your function:
    * lambda:GetAlias
    * lambda:UpdateAlias
    * lambda:CreateAlias  

## Task Output
The pipe will create a `aws-lambda-deploy-env` file under the `$BITBUCKET_PIPE_SHARED_STORAGE_DIR` directory when executing the 'update' command, containing the `FunciontArn`, `Version` and other function data in a json format.

The version can be read from this file, and used in later pipes or commands.

## Examples
### Update a Lambda
Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip'.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:0.5.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
```

Update the function 'my-lambda-function' in region 'us-east-1' with the function code contained in 'my-function-code.zip'. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.
```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:0.5.1
    variables:
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-function-code.zip'
```

### Create or Update an Alias
Update an alias named 'production' to point to version 2 of function 'my-lambda-function'
in region 'us-east-1':

```yaml
script:
  - pipe: atlassian/aws-lambda-deploy:0.5.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'alias'
      ALIAS: 'production'
      VERSION: '2'
```

### Combinining Update with Alias
A common use case is to update a Lambda function and publish a version, then point a new alias to the newly published function.
This allows you to create sophisticated Continuous Delivery workflows for your functions.

```yaml
script:
  
  # Build Lambda
  - build-lambda.sh
  
  # Update lambda code and publish a new version
  - pipe: atlassian/aws-lambda-deploy:0.5.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'update'
      ZIP_FILE: 'my-lambda.zip'
    
  # Read results from the update pipe into environment variables
  - VERSION=$(jq  --raw-output '.Version' $BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env)
  
  # Point an alias to the new lambda version
  - pipe: atlassian/aws-lambda-deploy:0.5.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      FUNCTION_NAME: 'my-lambda-function'
      COMMAND: 'alias'
      ALIAS: 'production'
      VERSION: '${VERSION}'       
```      
 
### Use Bitbucket Pipelines Deployments
In this scenario, we update and publish a Lambda, then use aliases to move the Lambda through test, staging and production deployment environments.

```yaml
- step:
    name: Build Lambda
    script:
    # Build Lambda
    - build-lambda.sh   
    # Update lambda code and publish a new version
    - pipe: atlassian/aws-lambda-deploy:0.5.1
      variables:
        AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
        AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
        AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
        FUNCTION_NAME: 'my-lambda-function'
        COMMAND: 'update'        
        ZIP_FILE: 'my-lambda.zip'   

- step:
    name: Deploy to Test  
    deployment: test
    script:
      # Point the test alias to the function.
      - pipe: atlassian/aws-lambda-deploy:0.5.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'test'
      # Test the function     
      - run-tests.sh
- step:
    name: Deploy to Staging   
    deployment: staging
    script:
      # Point the 'staging' alias to the function.
      - pipe: atlassian/aws-lambda-deploy:0.5.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'staging'
      # Test the function    
      - run-tests.sh
- step:
    name: Deploy to Production     
    deployment: production
    script:
      # Point the 'production' alias to the function.
      - pipe: atlassian/aws-lambda-deploy:0.5.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          FUNCTION_NAME: 'my-lambda-function'
          COMMAND: 'alias'
          ALIAS: 'production'
      # Test the function     
      - run-tests.sh      
``` 


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,lambda,serverless
