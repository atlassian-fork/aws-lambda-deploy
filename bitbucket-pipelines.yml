image:
  name: python:3.7


setup: &setup
  step:
    name: Setup testing resources
    script:
      - STACK_NAME="bbci-pipes-test-infrastructure-lambda-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-sam-deploy:0.2.3
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: 'us-east-1'
          S3_BUCKET: 'bbci-pipes-test-infrastructure-us-east-1-aws-lambda-deploy'
          STACK_NAME: ${STACK_NAME}
          SAM_TEMPLATE: './test/sam_template.yaml'
          CAPABILITIES: ['CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
          WAIT: 'true'


release-dev: &release-dev
  step:
    name: Release development version
    image: python:3.7
    script:
      - set -ex
      - pip install semversioner
      - VERSION=$(semversioner current-version)
      - IMAGE=bitbucketpipelines/$BITBUCKET_REPO_SLUG
      - echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
      - docker build -t ${IMAGE} .
      - docker tag ${IMAGE} ${IMAGE}:${VERSION}.${BITBUCKET_BUILD_NUMBER}-dev
      - docker push ${IMAGE}:${VERSION}.${BITBUCKET_BUILD_NUMBER}-dev
    services:
      - docker


test: &test
  parallel:
    - step:
        name: Test
        services:
        - docker
        script:
          - pip install -r test/requirements.txt
          - pytest test/test.py --verbose --capture=no --junitxml=test-reports/report.xml
        after-script:
          - STACK_NAME="bbci-pipes-test-infrastructure-lambda-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME}
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


push: &push
  step:
    name: Push and Tag
    script:
      - pipe: docker://bitbucketpipelines/bitbucket-pipe-release:1.0.0
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/${BITBUCKET_REPO_SLUG}


pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push
    qa-*:
    - <<: *test
    - <<: *push
  custom:
    test-and-report:
      - step:
          caches:
            - maven
          image: maven:3.3.9
          services:
            - docker
          script:
            - export DOCKERHUB_IMAGE='bitbucketpipelines/${BITBUCKET_REPO_SLUG}'
            - export DOCKERHUB_TAG=${BITBUCKET_BUILD_NUMBER}
            # Install bats, AWS CLI and shellcheck
            - apt-get update && apt-get install -y bats shellcheck python3-pip
            # Shell check
            - shellcheck pipe.sh
            # Install AWS CLI
            - apt-get update && apt-get install -y
            - pip3 install awscli --upgrade --user
            - export PATH=$PATH:/root/.local/bin/
            # Build the pipe
            - docker build -t ${DOCKERHUB_IMAGE}:${BITBUCKET_BUILD_NUMBER} -t ${DOCKERHUB_IMAGE}:latest .
            # Run tests
            - set +e
            - bats test/test*; test_exit_code=$?
            - set -e
            - echo test_exit_code = $test_exit_code
            - pipe: atlassian/report-task-test-result:1.0.0
              variables:
                DATADOG_API_KEY: $DATADOG_API_KEY
                TASK_NAME: aws-lambda
                TEST_EXIT_CODE: $test_exit_code
            - exit $test_exit_code
