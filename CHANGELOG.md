# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.5.0

- minor: Add default values for AWS variables.

## 0.4.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.4.2

- patch: Internal maintenance: Refactor tests to python.

## 0.4.1

- patch: Internal release

## 0.4.0

- minor: Support updating function configuration

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Refactor pipe code to use pipes bash toolkit.
- patch: Update aws-cli base docker image version.

## 0.3.0

- minor: Use pipes data sharing to store data

## 0.2.3

- patch: Minor documentation update

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- patch: Initial release of Bitbucket Pipelines AWS Lambda update pipe.

